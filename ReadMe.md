# Git Practice Tasks

## About Project

- This is a simple project containing following two .cpp files.
- main.cpp
- firstFunc.cpp

## Clone Project

- Copy the link of remote repo by pressing clone button.
- Open Terminal and run following commands.

```
cd "<path of directory where you want to clone>"                    # change directory to desired directory
git clone "<copied repository link>"                                # to clone repository
```

## Compilation of .cpp Files

- Execute following commands to compile both files
  
```
g++ -o mainOutput main.cpp                                          # to compile main.cpp file
g++ -o funcOutput func.cpp                                          # to compile func.cpp file
```

## Run .cpp Files

- Execute following commands to run both files

```
./mainOutput                                                        # to run main.cpp file
./funcOutput                                                        # to run func.cpp file
```